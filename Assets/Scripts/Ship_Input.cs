﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship_Input : MonoBehaviour
{
    public Vector2 axis;
    public Player_Behaviour player;

    // Update is called once per frame
    void Update() {
        axis.x = Input.GetAxis("Horizontal");
        axis.y = Input.GetAxis("Vertical");

        if (Input.GetButton("Fire1")){
            Debug.Log("Shooting");
            player.Shoot();
        }

        player.SetAxis(axis);
    }
}
