﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Behaviour : MonoBehaviour
{
    public float speed;
    private Vector2 axis;
    public Vector2 limits;

    public float shootTime = 0;
    public Weapon weapon;

    public Propeller prop;

    [SerializeField] AudioSource audioSource;
    [SerializeField] ParticleSystem ps;
    [SerializeField] Collider2D collider;
    [SerializeField] GameObject graphics;
    [SerializeField] ParticleSystem ps2;
    [SerializeField] Score_Manager ScoreManager;

    public int lives = 3;
    private bool iamDead = false;

    // Update is called once per frame
    void Update() {
        if (iamDead) {
            return;
        }

        shootTime += Time.deltaTime;

        transform.Translate(axis*speed*Time.deltaTime);

        if (transform.position.x > limits.x) {
            transform.position = new Vector3 (limits.x, transform.position.y, transform.position.z);
        } else if (transform.position.x < -limits.x) {
            transform.position = new Vector3 (-limits.x, transform.position.y, transform.position.z);
        }

        if (transform.position.y > limits.y) {
            transform.position = new Vector3 (transform.position.x, limits.y, transform.position.z);
        } else if (transform.position.y < -limits.y) {
            transform.position = new Vector3 (transform.position.x, -limits.y, transform.position.z);
        }
	
        if (axis.x > 0) {
        prop.BlueFire();
        } else {
            prop.Stop();
        }
		
		if (axis.x < 0) {
        prop.RedFire();
        } else {
            prop.Stop2();
        }
    }
    
    public void SetAxis (Vector2 currentAxis) {
        axis = currentAxis;
    }

    public void SetAxis (float x, float y) {
        axis = new Vector2 (x, y);
    }

    public void Shoot() {
        if (shootTime > weapon.GetCadencia()) {
            shootTime = 0f;
            weapon.Shoot();
        }
    }

    public void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Meteor") {
            StartCoroutine(DestroyShip());
        }
    }

    IEnumerator DestroyShip() {

        iamDead = true;

        //Me quito una vida
        lives--;

        //Desactivo el grafico
        graphics.SetActive(false);

        //Elimino el BoxCollider2D
        collider.enabled = false;

        //Lanzo la partícula
        ps.Play();

        //Lanzo sonido de explosion
        audioSource.Play();

        //Desactivo el propeller
        prop.gameObject.SetActive(false);

        //Indicamos al score que hemos perdido una vida+
        ScoreManager.LoseLife();

        //Me espero 1 segundo
        yield return new WaitForSeconds(1.0f);

        //Miro si tengo mas vidas
        if (lives > 0) {
            StartCoroutine(inMortal());
        }

        IEnumerator inMortal() {
            iamDead = false;
            graphics.SetActive(true);
            prop.gameObject.SetActive(true);

            for (int i =0; i<15;i++) {
                graphics.SetActive(false);
                yield return new WaitForSeconds(0.1f);
                graphics.SetActive(true);
                yield return new WaitForSeconds(0.1f);
            }

            collider.enabled = true;
        }

        if (lives == 1)
        {
            ps2.Play();
        }
    }
}
