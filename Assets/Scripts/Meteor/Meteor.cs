﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteor : MonoBehaviour
{
    public Vector2 speed;
    public GameObject[] graphics;
    int seleccionado;
    public AudioSource audioSource;
    public ParticleSystem ps;
    public GameObject MeteorToInstanciate;
    void Awake()
    {
        for (int i=0; i < graphics.Length; i++) {
            graphics[i].SetActive(false);
        }

        seleccionado = Random.Range(0, graphics.Length);
        graphics[seleccionado].SetActive(true);

        speed.x = Random.Range(-7, 0);
        speed.y = Random.Range(-4, 5);
    }

    void Update() {
        transform.Translate(speed * Time.deltaTime);
        graphics[seleccionado].transform.Rotate(0, 0, Random.Range(-100, 100) * Time.deltaTime);
    }
    public void OnTriggerEnter2D(Collider2D other)
    {
       if (other.tag == "Finish") {
           Destroy(this.gameObject);
       }
        else if (other.tag == "Bullet") {
            StartCoroutine(DestroyMeteor());
        }
    }

    IEnumerator DestroyMeteor()
    {
        graphics[seleccionado].SetActive(false);

        Destroy(GetComponent<BoxCollider2D>());

        ps.Play();

        audioSource.Play();

        InstanceMeteors();

        yield return new WaitForSeconds(1.0f);

        Destroy(this.gameObject);
    }

    public virtual void InstanceMeteors()
    {
        Instantiate(MeteorToInstanciate, this.transform.position, Quaternion.identity, null);
        Instantiate(MeteorToInstanciate, this.transform.position, Quaternion.identity, null);
        Instantiate(MeteorToInstanciate, this.transform.position, Quaternion.identity, null);
        Instantiate(MeteorToInstanciate, this.transform.position, Quaternion.identity, null);
    }
}

